﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task11
{
    class Program
    {
        static void Main(string[] args)
        {
            int year;
            Console.WriteLine("Type the year you want to check is a leap year");
            year = int.Parse(Console.ReadLine());

            if (year % 400 == 0)
                Console.WriteLine($"{year} is a leap");
            else if (year % 100 == 0)
                Console.WriteLine($"{year} is not a leap year");
            else if (year % 4 == 0)
                Console.WriteLine($"{year} is a leap year");
            else
                Console.WriteLine($"{year} is not a leap year");
           
        }
    }
    
}
