﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task34
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Type in number of weeks.");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine($"There is {n*5} working days in {n} weeks.");
        }
    }
}
