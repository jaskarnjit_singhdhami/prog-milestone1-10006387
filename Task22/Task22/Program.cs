﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task22
{
    class Program
    {
        static void Main(string[] args)
        {
            var fruitsVeges = new Dictionary<string, string>();

            fruitsVeges.Add("Tomato", "fruit");
            fruitsVeges.Add("Banana", "fruit");
            fruitsVeges.Add("Apple", "fruit");
            fruitsVeges.Add("Avocado", "fruit");
            fruitsVeges.Add("Peach", "fruit");
            fruitsVeges.Add("Lettuce", "vegetable");
            fruitsVeges.Add("Potato", "vegetable");
            fruitsVeges.Add("Carrot", "vegetable");
            fruitsVeges.Add("Broccoli", "vegetable");
            fruitsVeges.Add("Asparagus", "vegetable");

            Console.Write("There is a total of {0} fruits", fruitsVeges.Count(kvp => kvp.Value == "fruit"));
            Console.Write(" and {0} vegetables.", fruitsVeges.Count(kvp => kvp.Value == "vegetable"));
            Console.WriteLine();

            fruitsVeges.Remove("Lettuce");
            fruitsVeges.Remove("Potato");
            fruitsVeges.Remove("Carrot");
            fruitsVeges.Remove("Broccoli");
            fruitsVeges.Remove("Asparagus");

            foreach (var i in fruitsVeges)
            {
                Console.WriteLine(i);
            }
        }
    }
}
