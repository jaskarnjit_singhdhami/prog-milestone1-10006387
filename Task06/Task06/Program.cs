﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task06
{
    class Program
    {
        static void Main(string[] args)
        {
            var c = 5;
            int n = 0;

            for (int i = 0; i < c; i++)
            {
                Console.Write("Type a number for n=");
                n = n + int.Parse(Console.ReadLine());
            }
                       
            Console.WriteLine($"Total sum of numbers typed = {n}");
        }
    }
}