﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task02
{
    class Program
    {
        static void Main(string[] args)
        {
            int day = 0;

            Console.WriteLine("What month were you born?");
            var month = (Console.ReadLine());

            Console.WriteLine("What day of the month was it?");
            day = int.Parse(Console.ReadLine());

            Console.WriteLine($"You were born day {day} of the month {month}.");

        }
    }
}
