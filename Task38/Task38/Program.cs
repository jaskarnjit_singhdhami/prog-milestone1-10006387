﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task38
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Type a number to get times tables upto 12.");
            int x = int.Parse(Console.ReadLine());

            for (int y = 1; y <= 12; y++)
            {
                Console.WriteLine($"{y} x {x} = {y*x}");
            }
        }
    }
}
