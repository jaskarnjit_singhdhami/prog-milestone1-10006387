﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task40
{
    class Program
    {
        static void Main(string[] args)
        {
        
            var months = new SortedList<string, int>();


            months.Add("January", 31);
            months.Add("February", 29);
            months.Add("March", 31);
            months.Add("April", 30);
            months.Add("May", 31);
            months.Add("June", 30);
            months.Add("July", 31);
            months.Add("August", 31);
            months.Add("September", 30);
            months.Add("October", 31);
            months.Add("November", 30);
            months.Add("December", 31);

           
            int d = 0;

            Console.WriteLine($"Type a month (With capital first letter).");
            var x = Console.ReadLine();
               
            if (months.TryGetValue(x, out d))
            {
                if (d == 31)
                {
                    Console.WriteLine($"{x} has five Wednesdays.");
                }
                else
                {
                    Console.WriteLine($"{x} has four Wednesdays.");
               }
            }
        }
    }
}