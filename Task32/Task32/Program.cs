﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task32
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] days = new string [7] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
            
            int i = 1;

                foreach (string x in days)
                {
                    Console.WriteLine($"{x} is day {i++}.");
                }
           
        }
    }
}