﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task20
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = new int[7] { 33, 45, 21, 44, 67, 87, 86 };

            var result = numbers.Where(i => i % 2 != 0).ToArray();

            foreach (int odd in result)

                Console.WriteLine(odd);
        }
    }
}
