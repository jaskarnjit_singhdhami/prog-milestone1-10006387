﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task39
{
    class Program
    {
        static void Main(string[] args)
        {
            var months = new SortedList<string, int>();

            months.Add("January", 31);
            months.Add("February", 29);
            months.Add("March", 31);
            months.Add("April", 30);
            months.Add("May", 31);
            months.Add("June", 30);
            months.Add("July", 31);
            months.Add("August", 31);
            months.Add("September", 30);
            months.Add("October", 31);
            months.Add("November", 30);
            months.Add("December", 31);

            foreach (KeyValuePair<string,int> x in months)
            { 
               Console.WriteLine($"{x.Key} has 5 Mondays");
            }
        }
    }
}

