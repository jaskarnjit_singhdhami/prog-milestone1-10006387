﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task07
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("Four times table");

            for (int i = 4; i <= 4; i++)
            {
                for (int j = 1; j <= 12; j++)
                {
                    Console.WriteLine($"{i} x {j}={i * j}");
                }
            }
        }
    }
}
