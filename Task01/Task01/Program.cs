﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task01
{
    class Program
    {
        static void Main(string[] args)
        {
            var age = 0;

            Console.WriteLine("What is your name?");
            var name = Console.ReadLine();

            Console.WriteLine("What is your age?");
            age = int.Parse(Console.ReadLine());


            Console.WriteLine($"Hello {name} you are {age} years old.");
            Console.WriteLine("Hello {0} you are {1} years old." ,name, age);
            Console.WriteLine("Hello "+ name +" you are "+ age +" years old.");

        }
    }
}
