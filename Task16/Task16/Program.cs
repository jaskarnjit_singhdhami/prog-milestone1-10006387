﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task16
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Type a word so we can calculate how many letter it has");
            string word = (Console.ReadLine());
            int count = word.Length;
            Console.WriteLine($"{word} has {count} letters.");
        }
    }
}
