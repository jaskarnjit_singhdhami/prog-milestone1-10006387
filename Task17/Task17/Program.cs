﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task17
{
    class Program
    {
        static void Main(string[] args)
        {
            var people = new List<Tuple<string, int>>();
            people.Add(Tuple.Create("Skye", 32));
            people.Add(Tuple.Create("Bailey", 20));
            people.Add(Tuple.Create("Joey", 22));
            
            foreach(var y in people)
            {
                Console.WriteLine($"{y.Item1} is {y.Item2} years old.");
            }                
        }
    }
}
