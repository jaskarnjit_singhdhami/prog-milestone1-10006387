﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task04
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please type \"Cel\" for Celsius or \"Fah\" for Fahrenheit");
            var tempSelection = Console.ReadLine();


            switch (tempSelection)
            {
                case "Cel":
                    Console.WriteLine("Type a number to convert Celsius to Fahrenheit");
                    int c = int.Parse(Console.ReadLine());



                    int C2F = ((c * 9) / 5) + 32;
                    Console.WriteLine($"The temperature conversion is {C2F} °F");
                    break;



                case "Fah":
                    Console.WriteLine("Type a number to convert Fahrenheit to Celsius");
                    int f = int.Parse(Console.ReadLine());



                    int F2C = ((f - 32) / 9) * 5;
                    Console.WriteLine($"The temperature conversion is {F2C} °C");
                    break;
                default:
                    Console.WriteLine("You needed to type in \"Cel\" for Celsius or \"Fah\" for Fahrenheit - Please relaunch the program");
                    break;
            }
        }
    }
}
