﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task10
{
    class Program
    {
        static void Main(string[] args)
        {
            int startYear = 2016;
            int endYear = 2036;
            var leapYears = Enumerable.Range(startYear, endYear - startYear + 1)
                              .Count(x => DateTime.IsLeapYear(x));
            Console.WriteLine($"There is {leapYears} leap years in the next twenty years.");

        }
    }
}
